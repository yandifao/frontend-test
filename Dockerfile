FROM node:16.3.0-alpine3.13

RUN mkdir -p /usr/src/frontend

WORKDIR /usr/src/frontend

COPY package.json /usr/src/frontend

RUN npm install

COPY . /usr/src/frontend

EXPOSE 3000

CMD ["npm", "start"]